# -*- coding: UTF-8 -*-
# Subinitial Rapid Integration Platform (SiRIP) Kit
# © 2012-2020 Subinitial LLC. All Rights Reserved
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
# following conditions are met: Redistributions of source code and binary forms must retain the above copyright notice,
# these conditions, and the disclaimer below all in plain text. The name Subinitial may not be used to endorse or
# promote products derived from this software without prior written consent from Subinitial LLC. This software may only
# be redistributed and used with a Subinitial Stacks product.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. Refer to the included LICENSE.txt for more details.

from argparse import ArgumentError
from .landevice import *
import logging
import base64

logger = logging.getLogger(__name__)


PULL_NONE = 0
PULL_UP = 1
PULL_DOWN = 2

INPUT = 1
OUTPUT = 0


class BitSet(int):
    """An integer where each bit position (31-0) represents a Pin index, Channel index, Device index, etc.
        e.g. 0b1011 = represents indices 3, 1, & 0
    """
    pass


class GpioDirection(Scope):
    def __call__(self, ioconfig: BitSet):
        """Set direction of GPIO pins 0-15
        :param ioconfig: a bitset (16-bits) representing ioconfig of each pin (0=output, 1=input)
        """
        self._request(":gpio:direction", ioconfig)

    def get(self) -> BitSet:
        """Get directio of GPIO pins 0-15
        :return: a bitset (16-bits) representing ioconfig of each pin (0=output, 1=input)
        """
        resp = self._request(":gpio:direction?")
        return resp

    def outputs(self, output_pins: BitSet):
        """Configure pins to outputs
        :param output_pins: a bitset (16-bits) with each bit represening: 1=configure as ouptut, 0=no change
        """
        self._request(":gpio:direction_outputs", output_pins)

    def inputs(self, input_pins: BitSet):
        """Configure pins to outputs
        :param input_pins: a bitset (16-bits) with each bit represening: 1=configure as input, 0=no change
        """
        self._request(":gpio:direction_inputs", input_pins)


class Gpio(Scope):
    PIN_0 = BitSet(1 << 0)
    PIN_1 = BitSet(1 << 1)
    PIN_2 = BitSet(1 << 2)
    PIN_3 = BitSet(1 << 3)
    PIN_4 = BitSet(1 << 4)
    PIN_5 = BitSet(1 << 5)
    PIN_6 = BitSet(1 << 6)
    PIN_7 = BitSet(1 << 7)
    PIN_8 = BitSet(1 << 8)
    PIN_9 = BitSet(1 << 9)
    PIN_10 = BitSet(1 << 10)
    PIN_11 = BitSet(1 << 11)
    PIN_12 = BitSet(1 << 12)
    PIN_13 = BitSet(1 << 13)
    PIN_14 = BitSet(1 << 14)
    PIN_15 = BitSet(1 << 15)
    PIN_ALL = BitSet(0xFFFF)

    PULL_NONE = 0
    PULL_UP = 1
    PULL_DOWN = 2

    def __init__(self, dev):
        Scope.__init__(self, dev)
        self.direction = GpioDirection(dev)

    def get_state(self) -> BitSet:
        """Get state of GPIO pins 0-15
        :return: a bitset (16-bits) representing the state of the GPIO pins (0=low, 1=high)
        """
        resp = self._request(":gpio:status?")
        return resp

    def get_pin_state(self, pinset: BitSet) -> bool:
        """Get state of a pin
        :param pinset: pinset nominally representing ONE pin, see PIN_* values
        :return: True: if ANY pin represented in the pinset is high, False if all pins represented are low
        """
        return (self.get_state() & pinset) != 0

    def write(self, pinset: BitSet):
        """Write pinstate to all GPIO0-15 that are currently configured as outputs
        :param pinset: a bitset (16-bits) with each bit representing: 1=drive high, 0=drive low
        """
        self._request(":gpio:write", pinset)

    def set(self, pinset: BitSet):
        """Set output pins to drive high
        :param pinset: a bitset (16-bits) with each bit representing: 1=output high, 0=no change
        """
        self._request(":gpio:set", pinset)

    def clear(self, pinset: BitSet):
        """Clear output pins so they drive low
        :param pinset: a bitset (16-bits) with each bit representing: 1=output low, 0=no change
        """
        self._request(":gpio:clear", pinset)

    def command(self, pinset: BitSet, cmd: bool):
        """Command output pins so they drive based on cmd param
        :param pinset: a bitset (16-bits) with each bit representing: 1=output low, 0=no change
        :param cmd: if True, set, if False clear
        """
        if cmd:
            self.set(pinset)
        else:
            self.clear(pinset)

    def toggle(self, pinset: BitSet):
        """Toggle output pins
        :param pinset: a bitset (16-bits) with each bit representing: 1=toggle output state, 0=no change
        """
        self._request(":gpio:toggle", pinset)

    def get_command(self) -> BitSet:
        """Get output commanded state of pins
        :return: a bitset (16-bits) representing the commanded output state of the GPIO pins (0=low, 1=high)
        """
        resp = self._request(":gpio:command?")
        return resp

    def get_pin_command(self, pinset: BitSet) -> bool:
        """Get commanded state of specified GPIO output
        :param pinset: pinset nominally representing ONE pin, see PIN_* values
        :return: True = high, False = low
        """
        return self.get_command() & pinset > 0

    def configure_pull(self, pinset: BitSet, pullcfg: int):
        """Configure pin pad pull settings (weak pull-up/down resistors)
        :param pinset: a bitset (16-bits) with each bit representing: 1=set pin pull, 0=no change
        :param pullcfg: resistor pin pull setting, see PULL_* options
        """
        self._request(":gpio:configure_pull", pinset, pullcfg)

    def get_pull(self, pin_index: int) -> int:
        """
        :param pin_index: pin index (0-15) to retrieve the current pin pad pull setting
        :return: a pull value, see PULL_* options
        """
        if pin_index > 15:
            raise SubinitialInputError("pin_index should be 0-15 (hint: don't use a BitSet!)")
        resp = self._request(":gpio:get_pull?", pin_index)
        return resp

    def reset(self):
        """Reset to default power on state"""
        self.clear(self.PIN_ALL)
        self.direction.inputs(self.PIN_ALL)
        self.configure_pull(self.PIN_ALL, self.PULL_NONE)


class IsolatedInputs(Scope):
    INPUT_0 = 1 << 0
    INPUT_1 = 1 << 1
    INPUT_ALL = 0b11

    def __init__(self, dev):
        Scope.__init__(self, dev)
        self.direction = GpioDirection(dev)

    def get_state(self) -> BitSet:
        """Get status of isolated input channels 0-1
        :return: a bitset (2-bits) representing the state of the isolated input channels (0=low, 1=high)
        """
        resp = self._request(":iso:status?")
        return resp

    def get_pin_state(self, pinmask: BitSet) -> bool:
        """Get state of specified isolated input channel
        :param pinmask: pinmask nominally representing ONE pin, see INPUT_* values
        :return: True = high, False = low
        """
        return self.get_state() & pinmask > 0



class Adc(Scope):
    CHANNEL_COUNT = 7

    def __init__(self, dev):
        Scope.__init__(self, dev)
        self.direction = GpioDirection(dev)

    def measure(self, channel: int) -> float:
        """Measure pin-voltage of channels 0-6
        :return: a floating point voltage between 0 and 1.8V
        """
        if channel < 0 or channel > self.CHANNEL_COUNT:
            raise ArgumentError("channel", "must be between 0 and {}".format(self.CHANNEL_COUNT))
        resp = self._request(":adc:measure", channel)
        return resp

    def measure_all(self) -> list[float]:
        """Measure pin-voltage of channels 0-6 
        :return: a list of floating point voltages for each pin (including the VDD measurement as channel 7)
        """
        resp = self._request(":adc:measure_all")
        return resp

    def measure_vdd_3v3b(self) -> float:
        """Measure the VDD 3V3B supply rail
        :return: a floating point voltage near 3.3V
        """
        resp = self._request(":adc:measure", 7)
        if resp:
            return resp * 2
        return resp


class SolenoidDrivers(Scope):
    SOLENOID_0 = BitSet(1 << 0)
    SOLENOID_1 = BitSet(1 << 1)
    SOLENOID_ALL = BitSet(0b11)

    def write(self, pinset: BitSet):
        """Write pinstate to all solenoid drivers 0-1
        :param pinset: a bitset (2-bits) with each bit representing: 1=on, 0=off
        """
        self._request(":solenoid:write", pinset)

    def set(self, pinset: BitSet):
        """Set pins to on-state
        :param pinset: a bitset (2-bits) with each bit representing: 1=on, 0=no change
        """
        self._request(":solenoid:set", pinset)

    def clear(self, pinset: BitSet):
        """Clear output pins to off-state
        :param pinset: a bitset (2-bits) with each bit representing: 1=off, 0=no change
        """
        self._request(":solenoid:clear", pinset)

    def toggle(self, pinset: BitSet):
        """Toggle output states
        :param pinset: a bitset (2-bits) with each bit representing: 1=toggle output state, 0=no change
        """
        self._request(":solenoid:toggle", pinset)

    def get_command(self) -> BitSet:
        """Get commanded state of solenoid drivers
        :return: a bitset (2-bits) representing the commanded state of solenoid pins (0=off, 1=on)
        """
        resp = self._request(":solenoid:command?")
        return resp

    def get_pin_command(self, pinset: BitSet) -> bool:
        """Get drive state of specified solenoid driver
        :param pinset: pinset nominally representing ONE pin, see SOLENOID_* values
        :return: True = high, False = low
        """
        return self.get_command() & pinset > 0

    def reset(self):
        """Reset to default power on state"""
        self.clear(self.SOLENOID_ALL)


class SolidStateRelays(Scope):
    SSR_0 = BitSet(1 << 0)
    SSR_1 = BitSet(1 << 1)
    SSR_ALL = BitSet(0b11)

    def write(self, pinstate: BitSet):
        """Write output state to all solid-state relays 0-1
        :param pinstate: a bitset (2-bits) with each bit representing: 1=on, 0=off
        """
        self._request(":ssr:write", pinstate)

    def set(self, pinset: BitSet):
        """Set relays to on-state
        :param pinset: a bitset (2-bits) with each bit representing: 1=on, 0=no change
        """
        self._request(":ssr:set", pinset)

    def clear(self, pinset: BitSet):
        """Clear relays to off-state
        :param pinset: a bitset (2-bits) with each bit representing: 1=off, 0=no change
        """
        self._request(":ssr:clear", pinset)

    def toggle(self, pinset: BitSet):
        """Toggle relay states
        :param pinset: a bitset (2-bits) with each bit representing: 1=toggle output state, 0=no change
        """
        self._request(":ssr:toggle", pinset)

    def get_command(self) -> BitSet:
        """Get commanded state of solid state relays drivers
        :return: a bitset (2-bits) representing the commanded state of solid state relays (0=off, 1=on)
        """
        resp = self._request(":ssr:command?")
        return resp

    def get_pin_command(self, pinset: BitSet) -> bool:
        """Get state of specified relay
        :param pinset: pinset nominally representing ONE relay, see SSR_* values
        :return: True = high, False = low
        """
        return self.get_command() & pinset > 0

    def reset(self):
        """Reset to default power on state"""
        self.clear(self.SSR_ALL)


class DutPowers(Scope):
    def set_12v(self, state: bool):
        """Set dutpower on/off
        :param state: True=on, False=off
        """
        self._request(":dutpwr:set_12v", state)

    def get_12v(self) -> bool:
        """Get dutpower status
        :return: current dutpower channel state (True=on, False=off)
        """
        resp = self._request(":dutpwr:get_12v")
        return resp

    def set_5v(self, state: bool):
        """Set dutpower on/off
        :param state: True=on, False=off
        """
        self._request(":dutpwr:set_5v", state)

    def get_5v(self) -> bool:
        """Get dutpower status
        :return: current dutpower channel state (True=on, False=off)
        """
        resp = self._request(":dutpwr:get_5v")
        return resp

    def set_3v3(self, state: bool):
        """Set dutpower on/off
        :param state: True=on, False=off
        """
        self._request(":dutpwr:set_3v3", state)

    def get_3v3(self) -> bool:
        """Get dutpower status
        :return: current dutpower channel state (True=on, False=off)
        """
        resp = self._request(":dutpwr:get_3v3")
        return resp

    def set_neg12v(self, state: bool):
        """Set dutpower on/off
        :param state: True=on, False=off
        """
        self._request(":dutpwr:set_neg12v", state)

    def get_neg12v(self) -> bool:
        """Get dutpower status
        :return: current dutpower channel state (True=on, False=off)
        """
        resp = self._request(":dutpwr:get_neg12v")
        return resp

    def reset(self):
        """Reset to default power on state"""
        self.set_3v3(False)
        self.set_5v(False)
        self.set_12v(False)
        self.set_neg12v(False)


class Pwm(Scope):
    """
    Pulse Width Modulator
    """
    def __init__(self, dev, channel):
        Scope.__init__(self, dev)
        self.channel = channel

    def set_frequency(self, frequency: float):
        """Set PWM channel frequency
        :param frequency: value in Hz (from 1.0 to 10e6 Hz)
        """
        self._request(f":pwm{self.channel}:period", 1/frequency)

    def get_frequency(self) -> float:
        """Get PWM channel frequency
        :return: value in Hz (from 1.0 to 10e6 Hz)
        """

        resp = self._request(f":pwm{self.channel}:period?")
        if type(resp) == float:
            resp = round(1 / resp, 3)
        return resp

    def set_dutycycle(self, dutycycle: float):
        """Set PWM channel dutycycle
        :param dutycycle: value between 0-1.0
        """
        self._request(f":pwm{self.channel}:dutycycle", dutycycle)

    def get_dutycycle(self) -> float:
        """Get PWM channel dutycycle
        :return: value from 0-1.0
        """
        resp = self._request(f":pwm{self.channel}:dutycycle?")
        return round(resp, 3)

    def set_highz(self, state: bool):
        """Set highz ouptut state"""
        self._request(f":pwm{self.channel}:highz", int(state))

    def get_highz(self) -> bool:
        """Get highz ouptut state
        :return: True of on, False if off
        """
        resp = self._request(f":pwm{self.channel}:highz?")
        return bool(resp)

    def reset(self):
        """Reset to default power on state"""
        self.set_dutycycle(0.0)
        self.set_frequency(1e3)
        self.set_highz(False)

class FrequencyCounter(Scope):
    RANGE_HIGH_248_RISING_EDGES = 0
    RANGE_LOW_4_RISING_EDGES = 1

    def measure(self, range: int = RANGE_HIGH_248_RISING_EDGES, timeout: float = 3.0) -> float:
        """Measure frequency of input signal
        :param range: measurement range, refer to FrequencyCounter.RANGE_* options
        :param timeout: seconds after which to raise a SubinitialTimeoutError if no measurement can be made
        :return: frequency in Hz

            NOTE: this measure function requires 248 rising edges to make a frequency measurement in RANGE_HIGH* and
        4 rising-edge events of the signal in RANGE_LOW*. If less rising edges are experienced before the timeout
        this call will raise a SubinitialTimeoutError
        """
        div = (62, 1)[range]
        self._request(":ecap:restart", div)
        tstart = time.time()
        while True:
            period = self._request(":ecap:period?")
            if period >= 0:
                return 1/period
            if time.time() - tstart > timeout:
                raise SubinitialTimeoutError(f"frequency measurement failed, <4 rising edges detected in {timeout}s")

    def measure_pwm(self, timeout: float = 0.05) -> Tuple[float, float]:
        """Measure PWM frequency and duty-cycle of an input waveform
        :param timeout: seconds after which to assume the signal is DC: {frequency: -1.0, duty_cycle: 1.0 or 0.0}

        :return: (frequency: float, duty_cycle: float) where
            - frequency is in Hz, and if a DC value is assumed it will be -1Hz,
            - duty_cycle is a ratio from 0-1 indicating the portion of the period that the waveform spends HIGH
        """
        self._request(":ecap:restart", 1, True)
        tstart = time.time()
        while True:
            frequency, duty_cycle = self._request(":ecap:pwm?")
            if frequency < 0 and ((time.time() - tstart) < timeout):
                time.sleep(0.003)
                continue
            return float(frequency), float(duty_cycle)

    

class UartSocket(socket.socket):
    TimeoutError = socket.timeout

    def __init__(self, family, type, proto=0):
        socket.socket.__init__(self, family, type, proto)
        self.port = -1

    def recv(self, bufsize=4096, flags=0, timeout=None):
        if self.gettimeout() != timeout:
            self.settimeout(timeout)
        try:
            rxd = socket.socket.recv(self, bufsize, flags)
        except BlockingIOError:
            return b''
        except socket.timeout:
            return b''
        if not rxd:
            raise SubinitialResponseError("Server closed this UartSocket connection")
        return rxd

    def recvall(self, bufsize, flags=0, timeout=None):
        chunks = []
        tstart = time.monotonic()
        while True:
            chunk = self.recv(bufsize, flags, timeout)
            bufsize -= len(chunk)
            chunks.append(chunk)
            if not bufsize:
                break
            if timeout is not None:
                if time.monotonic() - tstart > timeout:
                    break
        return b''.join(chunks)

    def clear(self):
        timeout = self.gettimeout()
        self.settimeout(0)
        try:
            d = True
            while d:
                d = socket.socket.recv(self, 4096)
        except:
            pass
        self.settimeout(timeout)


class Uarts(Scope):
    UART_0 = 0
    UART_1 = 1
    UART_2 = 2
    UART_3 = 3

    def _rs485(self, port: int, state: bool):
        """Configure rs485 mode for a channel
        :param port: UART port 0-3
        :param state: True=on, False=off
        """
        self._request(":uarts:rs485", port, state)

    def _get_rs485(self, port: int) -> bool:
        """Get rs485 mode configuration for a channel
        :param port: UART port 0-3
        :return: state True=on, False=off
        """
        resp = self._request(":uarts:rs485?", port)
        return resp

    def baudrate(self, port: int, baud: int):
        """ set baud-rate for the specified port
        The default baud-rates at bootup are 115200
        :param port: UART port 0-3
        :param baud: new baud rate in bits-per-second
        """
        self._request(":uarts:baudrate", port, baud)

    def get_baudrate(self, port: int) -> int:
        """get baud-rate for the specified port
        The default baud-rates at bootup are 115200
        :param port: UART port 0-3
        :return: baud-rate in bits-per-second
        """
        resp = self._request(":uarts:baudrate?", port)
        return resp

    PARITY_NONE = "N"
    PARITY_EVEN = "E"
    PARITY_ODD = "O"
    PARITY_MARK = "M"
    PARITY_SPACE = "S"

    def parity(self, port: int, parity: str):
        """
        Set parity setting of the specified port
        By default all ports start with 'N' PARITY_NONE
        :param port: UART port 0-3
        :param newparity: 'N', 'E', 'O', 'M', or 'S' which correspond to
            PARITY_NONE, PARITY_EVEN, PARITY_ODD, PARITY_MARK, PARITY_SPACE
        :return:
        """
        self._request(":uarts:parity", port, parity)

    def get_parity(self, port: int) -> str:
        """
        Get parity setting of the specified port
        By default all ports start with 'N' PARITY_NONE
        :param port: UART port 0-3
        :return: 'N', 'E', 'O', 'M', or 'S' which correspond to
            PARITY_NONE, PARITY_EVEN, PARITY_ODD, PARITY_MARK, PARITY_SPACE
        """
        resp = self._request(":uarts:parity?", port)
        return resp

    def make_socket(self, port: int) -> UartSocket:
        """
        Create a new connected socket for the specified serial port
        :param port: UART port 0-3
        :return: socket open and ready for sending/receiving data

        UartSocket
            Compared to a socket.socket, the UartSocket
            has one modified method and one new method:

            1) sock.recv(buffsize: int, flags:int, timeout: float or None) -> bytes
                timeout: added parameter that Blocks for timeout seconds or indefinitely if None
                         if timeout occurs, this function returns an empty bytestring: b''

            3) sock.clear()
                immediately clears any buffered received data without raising exceptions
        """
        sock = UartSocket(socket.AF_INET, socket.SOCK_STREAM)
        sock.port = port
        sock.settimeout(5)
        if port < 10:
            port += 8800
        sock.connect((self._dev.connection.ipaddr, port))

        sock.settimeout(None)
        return sock

    def reset(self):
        """Reset to default power on state"""
        for port in [self.UART_0, self.UART_1, self.UART_2, self.UART_3]:
            self.baudrate(port, 115200)
            self.parity(port, self.PARITY_NONE)


_canfmt = "<IB3x8s"


class CanSocket(socket.socket):
    CAN_EFF_FLAG = 0x80000000

    TimeoutError = socket.timeout

    def __init__(self, family, type, proto=0):
        socket.socket.__init__(self, family, type, proto)
        self.port = -1

    def recv_frame(self, timeout: float|None=None) -> tuple[int, bytes]|tuple[None, None]:
        if self.gettimeout() != timeout:
            self.settimeout(timeout)
        try:
            c = self.recv(16)
        except BlockingIOError:
            return None, None
        except socket.timeout:
            return None, None
        if not c:
            raise SubinitialResponseError("Server closed this CanSocket connection")
        if len(c) == 16:
            can_id, length, data = struct.unpack(_canfmt, c)
            return can_id, data[:length]
        raise SubinitialResponseError("Invalid CAN packet: %s".format(c))

    def send_frame(self, cob_id, data, flags=0):
        db = data + b'\x00\x00\x00\x00\x00\x00\x00\x00'
        c = struct.pack(_canfmt, cob_id, len(data), db[:8])
        self.send(c, flags)

    def clear(self):
        timeout = self.gettimeout()
        self.settimeout(0)
        try:
            d = True
            while d:
                d = socket.socket.recv(self, 16*200)
        except:
            pass
        self.settimeout(timeout)


class Can(Scope):
    PORT = 8811

    def bitrate(self, newbitrate: float):
        """ set bit-rate for the specified port
        The default bit-rate at bootup is 1Mbps
        :param newbitrate: new baud rate in bits-per-second
        """
        self._request(":cans:bitrate", self.PORT, int(newbitrate))

    def get_bitrate(self) -> float:
        """get bit-rate for the can port
        The default bit-rate at bootup is 1Mbps
        :return: bit-rate in bits-per-second
        """
        resp = self._request(":cans:bitrate?", self.PORT)
        return resp

    def make_socket(self) -> CanSocket:
        """
        Create a new connected socket for the CAN port
        :return: socket open and ready for sending/receiving data

        CAN Sockets
            a CanSocket will be made which has
            three new methods in addition to all socket.socket methods:

            1) sock.recv_frame(timeout: float or None) -> (int, bytes)
                receives the next CAN frame read on the bus

                timeout: Blocks for timeout seconds or indefinitely if None
                         WARNING: if timeout occurs a socket.timeout will be raised
                return: (cob_id, data) CAN Frame.
                    cob_id = function code (4Msb), node ID (7b), and RTR (1Lsb)
                    data = bytes of CAN Frame Data Length (between 0 and 8 bytes)

            2) sock.send_frame(cob_id: int, data: bytes):
                sends the can frame cob_id+data on the bus

                cob_id: CANOpen Communication Object Identifier (lower 12bits total)
                    function code (4Msb), node ID (7b), and RTR (1Lsb)

                data: valid data bytes from the CAN payload, len(data) will be
                    equal to the Data Length value of the CANOpen Frame.

            3) sock.clear()
                immediately clears any buffered received frames without raising exceptions
        """
        port = self.PORT
        sock = CanSocket(socket.AF_INET, socket.SOCK_STREAM)
        sock.port = port
        sock.settimeout(5)
        if port < 10:
            port += self.PORT
        sock.connect((self._dev.connection.ipaddr, port))
        sock.settimeout(None)
        return sock

    def reset(self):
        """Reset to default power on state"""
        self.bitrate(1e6)


class I2C(Scope):
    CLOCKSPEED_400kHZ = 400000
    CLOCKSPEED_100kHZ = 100000
    CLOCKSPEED_10kHZ = 10000

    def __init__(self, dev, port):
        Scope.__init__(self, dev)
        self.port = port

    def clockspeed(self, frequency_hz: int):
        """Set the i2c bus clockspeed
        :param frequency_hz: i2c clock frequency in Hz
        """
        self._request(f"i2c{self.port}:clockspeed", frequency_hz)

    def get_clockspeed(self) -> int:
        """Get i2c bus's clockspeed setting
        :return: i2c bus clockspeed in Hz
        """
        resp = self._request(f"i2c{self.port}:clockspeed?")
        return resp

    def write(self, slave_id: int, wdata: bytes):
        """Write data to selected slave
        :param slave_id: i2c device slave ID to be communicated with
        :param wdata: data to be written to slave
        """
        wdata64 = base64.b64encode(wdata).decode("ascii")
        failed = False
        try:
            self._request(f"i2c{self.port}:write64", slave_id, wdata64)
        except SubinitialResponseError:
            failed = True
        if failed:
            raise SubinitialResponseError("i2c.write failed, invalid communication with slave: {}".format(slave_id))

    def read(self, slave_id: int, read_length: int) -> bytes:
        """Read bytes from selectd slave
        :param slave_id: i2c device slave ID to be communicated with
        :param read_length: number of bytes to read from the slave
        :return: bytes read from the selected slave
        """
        failed = False
        try:
            resp = self._request(f"i2c{self.port}:read64", slave_id, read_length)
            if resp.startswith("error:"):
                return b''
        except SubinitialResponseError:
            failed = True
        if failed:
            raise SubinitialResponseError("i2c.read failed, invalid communication with slave: {}".format(slave_id))
        return base64.b64decode(resp.encode("ascii"))

    def write_read(self, slave_id: int, wdata: bytes, read_length: int) -> bytes:
        """Write data to selected slave then read bytes back all within one message (e.g. smbus / pmbus messages)
        :param slave_id: i2c device slave ID to be communicated with
        :param wdata: data to be written to slave
        :param read_length: number of bytes to read from the slave
        """
        wdata64 = base64.b64encode(wdata).decode("ascii")
        failed = False
        try:
            resp = self._request(f"i2c{self.port}:write_read64", slave_id, wdata64, read_length)
            if resp.startswith("error:"):
                return b''
        except SubinitialResponseError:
            failed = True
        if failed:
            raise SubinitialResponseError("i2c.write_read failed, invalid communication with slave: {}".format(slave_id))
        
        return base64.b64decode(resp.encode("ascii"))

    def reset(self):
        """Reset to default power on state"""
        self.clockspeed(self.CLOCKSPEED_100kHZ)


class Spi(Scope):
    MODE_CPHA = 0x1
    MODE_CPOL = 0x2
    MODE_0 = 0
    MODE_1 = MODE_CPHA
    MODE_2 = MODE_CPOL
    MODE_3 = MODE_CPHA | MODE_CPOL
    MODE_CS_HIGH = 0x4

    CS_PIN = 16  # SPI_CS labeled on the breakout board
    CS_GPIO_0 = 0
    CS_GPIO_1 = 1
    CS_GPIO_2 = 2
    CS_GPIO_3 = 3
    CS_GPIO_4 = 4
    CS_GPIO_5 = 6
    CS_GPIO_7 = 7
    CS_GPIO_8 = 8
    CS_GPIO_9 = 9
    CS_GPIO_10 = 10
    CS_GPIO_11 = 11
    CS_GPIO_12 = 12
    CS_GPIO_13 = 13
    CS_GPIO_14 = 14
    CS_GPIO_15 = 15

    def __init__(self, dev):
        Scope.__init__(self, dev)

    def clockspeed(self, frequency_hz: int|float):
        """Set the spi bus clockspeed
        :param frequency_hz: clock frequency in Hz
        """
        self._request(":spi:clockspeed", int(frequency_hz))

    def get_clockspeed(self) -> int:
        """Get spi bus's clockspeed setting
        :return: bus clockspeed in Hz
        """
        resp = self._request(":spi:clockspeed?")
        return resp

    def bits_per_word(self, bits_per_word: int):
        """Set the spi bus bits_per_word
        :param bits_per_word: bits per SPI word (6-32)
        """
        self._request(":spi:bits_per_word", bits_per_word)

    def get_bits_per_word(self) -> int:
        """Get spi bus's bits_per_word setting
        :return: bus bits_per_word setting (6-32)
        """
        resp = self._request(":spi:bits_per_word?")
        return resp

    def mode(self, mode: int):
        """Set the spi bus's mode
        :param mode: SPI mode bitmask of MODE_* settings
        """
        self._request("spi:mode", mode)

    def get_mode(self) -> int:
        """Get spi bus's mode setting
        :return: bus mode setting (bitmask of MODE_* settings)
        """
        resp = self._request("spi:mode?")
        return resp

    def chipselect(self, cs: int):
        """Set the SPI bus's current chipselect
        :param cs: current chipselect setting (0-16) see spi.CS_* options

        Note: if you use the GPIO pins as chipselects then it is your reponsibity to configure
           the GPIO pins as outputs and set them to the de-asserted state before you use SPI transfers.
           The RIP's SPI module will not take over control of the GPIO pin, but instead it will simply
           command the GPIO pin to the appropriate CS asserted/deasserted states as if the user wrote gpio.write(...)
           just before and after each spi transfer.
        """
        self._request(":spi:chipselect", cs)

    def get_chipselect(self) -> int:
        """Get spi bus's chipselect setting
        :return: current chipselect setting (0-16) see spi.CS_* options
        """
        resp = self._request(":spi:chipselect?")
        return resp

    def reset(self):
        """Reset to default power on state"""
        self.chipselect(self.CS_PIN)
        self.clockspeed(1000000)
        self.bits_per_word(8)
        self.mode(self.MODE_0)
        

    DEFAULT = 0
    PREVIOUS = -1
    
    class Segment:
        FLAG_POLL = 1
        DEFAULT = 0
        PIN_CS = 16

        def __init__(self, txd: bytes, cs=PIN_CS, flags=DEFAULT, config=DEFAULT, delay_sclk=DEFAULT, delay_word=DEFAULT, delay_ncs=DEFAULT, delay_after=DEFAULT):
            self.txd = txd
            """transmit data + length"""
            self.cs = cs
            """chipselect (by default it is the primary RipKit SPI chip select, integers 0-15 correspond to GPIO pins"""
            self.flags = flags
            """Optional flags to modify spi transmission behavior"""
            self.config = config
            """SPI segment configuration"""
            self.delay_sclk = delay_sclk
            """Delay in us (0-65535) while chip-select is asserted but before first sclk is pulsed"""
            self.delay_word = delay_word
            """Delay in us (0-65535) between each subsequent txrx word"""
            self.delay_ncs = delay_ncs
            """Delay in us (0-65535) before chip-select is de-asserted"""
            self.delay_after = delay_after
            """Delay in us (0-65535) after chip-select is de-asserted and whole transmission is complete before the next segment starts"""
            self.rxd = b''
            """Data received in response to txd payload (populated after spi.transfer has completed)"""
        
        class PollResult:
            def __init__(self, rxd: bytes):
                l = len(rxd)
                self.rxd = rxd[:l-8]
                self.match = rxd[l-8] == 0
                self.rx_poll_char = rxd[l-7]
            
            def __repr__(self):
                rxc = self.rx_poll_char
                return f"PollResult(match={self.match}, rxc={rxc}/{hex(rxc)}, rxd={self.rxd})"

        def get_poll_result(self) -> PollResult:
            return self.PollResult(self.rxd)
        
        def poll_until(self, expected: int, count: int, txb=0):
            self._poll(expected, txb, True, count)

        def poll_while(self, expected: int, count: int, txb=0):
            self._poll(expected, txb, False, count)
        
        def _poll(self, expected: int, txb: int, match_or_not: bool, count: int):
            self.flags |= self.FLAG_POLL
            self.txd = struct.pack("<BBBxI", expected, txb, match_or_not, count) + self.txd

        def configure(self, fclk: float, mode: int) -> 'Spi.Segment':
            """
            Build a configuration register value
            @param fclk: bus clockspeed in Hz
            @param mode: SPI mode bitmask of MODE_* settings
            @returns: resulting self.config value
            """
            fdiv = round(48e6 / fclk)
            if fdiv > 4096:
                raise ArgumentError("fclk must be between 24kHz and 24MHz")
            
            self.config = mode | ((fdiv & 0xf) << 2) | ((fdiv & 0xFF0) << (16-4))
            return self

        def _serialize(self) -> tuple:
            return (base64.b64encode(self.txd).decode("ascii"), self.cs, self.flags, self.config, self.delay_sclk, self.delay_word, self.delay_ncs, self.delay_after)

    def transfer(self, txd: bytes, cs=CS_PIN, flags=DEFAULT, config=DEFAULT, delay_sclk=DEFAULT, delay_word=DEFAULT, delay_ncs=DEFAULT, delay_after=DEFAULT) -> bytes:
        """Transmit and receive a single data transfer to the cs chip.
        :param txd_segment: segment of data to transmit on the bus
        :param cs: chipselect to assert during the transfer
        :param flags, config, delay_*: See Spi.Segment parameters...
        :return: received data from the transfer
        """
        return self.transfer_multi(self.Segment(txd, cs, flags, config, delay_sclk, delay_word, delay_ncs, delay_after))[0].rxd

    def transfer_multi(self, *segments: Segment) -> tuple[Segment, ...]:
        """
        Transmit and receive one or more segments on the SPI bus
        @param segments: set of tx/rx segments to be sent to the SPI bus
        @returns: received data from each transfer in a list
        """
        resp = self._request(f"spi:transfer8_64", tuple(s._serialize() for s in segments))
        # stat = resp[0]
        # print("transfer_multi_stat", resp)

        for s, r in zip(segments, resp[1:]):
            s.rxd = base64.b64decode(r.encode("ascii"))
        return tuple(segments)


class RipKit(LanDevice):
    ASSEMBLY = "SA14109"
    FIRMWARE_MIN = (0, 1, 0)

    NO_CHANGE = NO_CHANGE
    """do not change device value"""

    def __init__(self, host: str=None, autoconnect=True):
        """Construct a Subinitial SiRIP instance"""
        LanDevice.__init__(self, host, autoconnect)

        # Scopes
        self.gpio = Gpio(self)
        self.isolated_inputs = IsolatedInputs(self)
        self.solenoid_drivers = SolenoidDrivers(self)
        self.solid_state_relays = SolidStateRelays(self)
        self.dut_powers = DutPowers(self)
        self.pwm0 = Pwm(self, 0)
        self.pwm1 = Pwm(self, 1)
        self.frequency_counter = FrequencyCounter(self)
        self.uarts = Uarts(self)
        self.can = Can(self)
        self.i2c0 = I2C(self, 0)
        self.i2c1 = I2C(self, 1)
        self.spi = Spi(self)
        self.adc = Adc(self)

    def reset(self):
        '''Reset device to default power on state'''
        self.gpio.reset()
        self.solenoid_drivers.reset()
        self.solid_state_relays.reset()
        self.dut_powers.reset()
        self.pwm0.reset()
        self.pwm1.reset()
        self.uarts.reset()
        self.can.reset()
        self.i2c0.reset()
        self.i2c1.reset()
        self.spi.reset()
