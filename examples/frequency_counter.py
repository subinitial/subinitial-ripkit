# -*- coding: UTF-8 -*-
# SiRIP Kit Frequency Counter example
# © 2012-2020 Subinitial LLC. All Rights Reserved
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
# following conditions are met: Redistributions of source code and binary forms must retain the above copyright notice,
# these conditions, and the disclaimer below all in plain text. The name Subinitial may not be used to endorse or
# promote products derived from this software without prior written consent from Subinitial LLC. This software may only
# be redistributed and used with a Subinitial Stacks product.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. Refer to the included LICENSE.txt for more details.

from subinitial.ripkit import RipKit

""" Connect to the device """
# You can access the device by IP address (e.g. 192.168.1.49) or mDNS name
# For mDNS on windows, make sure you have "Bonjour Print Services for Windows" installed
#   Find it at: https://developer.apple.com/bonjour/
serial_number = "0617"
mdns_hostname = f"ripkit{serial_number}.local"
sirip = RipKit(mdns_hostname)


""" BASIC USAGE """

""" Try to measure the frequency of the signal within 3 seconds """
# NOTE: the frequency_counter requires 248 rising edges to make a frequency measurement in RANGE_HIGH and
#   4 rising-edge events of the signal in RANGE_LOW
print("Measuring Frequency...")
fcounter = sirip.frequency_counter
signal_hz = fcounter.measure(range=fcounter.RANGE_HIGH_248_RISING_EDGES, timeout=3.0)
print("Measured Frequency: {0}".format(signal_hz))


""" Try to measure the PWM frequency and duty-cycle of a signal within one period (50ms max)"""
pwm_hz, pwm_duty = fcounter.measure_pwm()
print("Measured PWM Frequency: {0}Hz, Duty-Cycle: {1}%".format(pwm_hz, pwm_duty * 100))
