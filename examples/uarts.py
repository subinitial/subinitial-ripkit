# -*- coding: UTF-8 -*-
# SiRIP Kit UART example
# © 2012-2020 Subinitial LLC. All Rights Reserved
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
# following conditions are met: Redistributions of source code and binary forms must retain the above copyright notice,
# these conditions, and the disclaimer below all in plain text. The name Subinitial may not be used to endorse or
# promote products derived from this software without prior written consent from Subinitial LLC. This software may only
# be redistributed and used with a Subinitial Stacks product.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. Refer to the included LICENSE.txt for more details.

import time
from subinitial.ripkit import RipKit

""" Connect to the device """
# You can access the device by IP address (e.g. 192.168.1.49) or mDNS name
# For mDNS on windows, make sure you have "Bonjour Print Services for Windows" installed
#   Find it at: https://developer.apple.com/bonjour/
serial_number = "0616"
mdns_hostname = f"ripkit{serial_number}.local"
sirip = RipKit(mdns_hostname)


""" BASIC USAGE """

""" Port Configuration """
UART_PORT = sirip.uarts.UART_3  # Any UART port 0 - 3
sirip.uarts.baudrate(UART_PORT, 115200)
sirip.uarts.parity(UART_PORT, sirip.uarts.PARITY_EVEN)

""" Open a socket connection to the device"""
uartsock = sirip.uarts.make_socket(UART_PORT)


""" Transmit a message """
txdata = b'xyz123'
uartsock.send(txdata)
print(f"Sending {txdata} on UART{uartsock.port}.")

time.sleep(0.1)

""" Retrieve any data received in the past 0.1 second """
while True:
    rxdata = uartsock.recv(timeout=0)  # don't block, timeout = 0
    if not rxdata:
        print("All received UART data processed.")
        break  # rxdata would == b'' in this case
    print(f"UART{uartsock.port} received: {rxdata}")

""" Start another thread to listen in for receive data as they appear """
import threading


def handle_uartrx():
    print(f"Listening for UART{uartsock.port} rxdata...")
    while True:
        rxdata = uartsock.recv(timeout=None)  # wait indefinitely for new rx data
        print(f"handle_uartrx: UART{uartsock.port} received: {rxdata}")


""" Clear any pre-existing data received on the UART RX Line """
uartsock.clear()

""" Start the listening thread """
rx_thread = threading.Thread(target=handle_uartrx)
rx_thread.daemon = True
rx_thread.start()

input("Press enter to quit >\n")
