# -*- coding: UTF-8 -*-
# SiRIP Kit SPI example
# © 2012-2020 Subinitial LLC. All Rights Reserved
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
# following conditions are met: Redistributions of source code and binary forms must retain the above copyright notice,
# these conditions, and the disclaimer below all in plain text. The name Subinitial may not be used to endorse or
# promote products derived from this software without prior written consent from Subinitial LLC. This software may only
# be redistributed and used with a Subinitial Stacks product.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. Refer to the included LICENSE.txt for more details.

from subinitial.ripkit import RipKit

""" Connect to the device """
# You can access the device by IP address (e.g. 192.168.1.49) or mDNS name
# For mDNS on windows, make sure you have "Bonjour Print Services for Windows" installed
#   Find it at: https://developer.apple.com/bonjour/
serial_number = "0617"
mdns_hostname = f"ripkit{serial_number}.local"
sirip = RipKit(mdns_hostname)


""" BASIC USAGE """

""" Set default bus parameters """
spi = sirip.spi
spi.clockspeed(1e6)  # 1MHz
spi.bits_per_word(8)
spi.mode(spi.MODE_CPOL | spi.MODE_CPHA)  # set to mode 3 spi.MODE_3

""" Transmit and receive one message (CS is asserted for the whole transfer) """
chipselect = spi.CS_PIN
txdata = b'xyz123'
rxdata = spi.transfer(txdata, cs=chipselect)
print(f"SPI transmitted {txdata}, device responded with: {rxdata}") # Note: unconnected MISO line may read sporatic data


""" ADVANCED USAGE """

""" Transmit and receive up to 8 messages with CS being strobed between each transfer """
txdata0, txdata1, txdata2 = b'xyz', b'123', b'asd'
rxdata0, rxdata1, rxdata2 = spi.transfer_multi([b'xyz', b'123', b'asd'])
print(f"SPI Master transmitted:\t{txdata0} -> {txdata1} -> {txdata2}\n"
      f"    Device responded:\t{rxdata0} -> {rxdata1} -> {rxdata2}")

""" Transmit and receive to a chip-select attached to one of the RIP Kit's GPIO pins """
#  Note: if you use the GPIO pins as chipselects then it is your responsibility to configure
#        the GPIO pins as outputs and set them to the de-asserted state before you use SPI transfers.
#        The RIP's SPI module will not take over control of the GPIO pin, but instead it will simply
#        command the GPIO pin to the appropriate CS asserted/de-asserted states as if the user wrote gpio.write(...)
#        just before and after each spi transfer.
gpio = sirip.gpio
gpio.direction.outputs(gpio.PIN_14)
gpio.set(gpio.PIN_14)
rxdata = spi.transfer(txdata, cs=spi.CS_GPIO_14)
print(f"SPI transmitted {txdata}, device with CS as GPIO_14 responded with: {rxdata}")
