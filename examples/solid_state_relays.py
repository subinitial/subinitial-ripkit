# -*- coding: UTF-8 -*-
# SiRIP Kit Solid State Relays example
# © 2012-2020 Subinitial LLC. All Rights Reserved
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
# following conditions are met: Redistributions of source code and binary forms must retain the above copyright notice,
# these conditions, and the disclaimer below all in plain text. The name Subinitial may not be used to endorse or
# promote products derived from this software without prior written consent from Subinitial LLC. This software may only
# be redistributed and used with a Subinitial Stacks product.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. Refer to the included LICENSE.txt for more details.

from subinitial.ripkit import RipKit

""" Connect to the device """
# You can access the device by IP address (e.g. 192.168.1.49) or mDNS name
# For mDNS on windows, make sure you have "Bonjour Print Services for Windows" installed
#   Find it at: https://developer.apple.com/bonjour/
serial_number = "0617"
mdns_hostname = f"ripkit{serial_number}.local"
sirip = RipKit(mdns_hostname)


""" BASIC USAGE """

""" Activate a solid state relay """
ssrs = sirip.solid_state_relays
ssrs.set(ssrs.SSR_1)

""" Check state of solid state relay """
print("SSR1 is active:", ssrs.get_pin_command(ssrs.SSR_1))

""" Clear all solid state relays """
ssrs.clear(ssrs.SSR_ALL)


""" ADVANCED USAGE """

""" Check state of both solid state relays simultaneously """
print("Relay command bitset:", hex(ssrs.get_command()))
