# -*- coding: UTF-8 -*-
# SiRIP Kit I2C example
# © 2012-2020 Subinitial LLC. All Rights Reserved
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
# following conditions are met: Redistributions of source code and binary forms must retain the above copyright notice,
# these conditions, and the disclaimer below all in plain text. The name Subinitial may not be used to endorse or
# promote products derived from this software without prior written consent from Subinitial LLC. This software may only
# be redistributed and used with a Subinitial Stacks product.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. Refer to the included LICENSE.txt for more details.

from subinitial.ripkit import RipKit

""" Connect to the device """
# You can access the device by IP address (e.g. 192.168.1.49) or mDNS name
# For mDNS on windows, make sure you have "Bonjour Print Services for Windows" installed
#   Find it at: https://developer.apple.com/bonjour/
serial_number = "0617"
mdns_hostname = f"ripkit{serial_number}.local"
sirip = RipKit(mdns_hostname)


""" BASIC USAGE """

""" Configure an I2C bus """
i2c = sirip.i2c0  # or sirip.i2c1
# configure the bus parameters
i2c.clockspeed(i2c.CLOCKSPEED_100kHZ)
SLAVE_ID = 0b1110101


""" Simple I2C write to slave """
i2c.write(SLAVE_ID, b'xyz123')


""" Simple I2C read 5 bytes from slave """
rxdata = i2c.read(SLAVE_ID, read_length=5)
print("read 5Bytes", rxdata)


""" ADVANCED USAGE """

""" SMBUS style write-then-read in one message """
import struct
cmd_register = b'\x12'
register32bit = i2c.write_read(SLAVE_ID, cmd_register, read_length=4)
# intepret the 32 bit regsiter value as big-endian
print("Register 0x12 Reads:", struct.unpack(">I", register32bit)[0])
