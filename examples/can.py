# -*- coding: UTF-8 -*-
# SiRIP Kit CAN Example
# © 2012-2020 Subinitial LLC. All Rights Reserved
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
# following conditions are met: Redistributions of source code and binary forms must retain the above copyright notice,
# these conditions, and the disclaimer below all in plain text. The name Subinitial may not be used to endorse or
# promote products derived from this software without prior written consent from Subinitial LLC. This software may only
# be redistributed and used with a Subinitial Stacks product.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. Refer to the included LICENSE.txt for more details.

from subinitial.ripkit import RipKit

""" Connect to the device """
# You can access the device by IP address (e.g. 192.168.1.49) or mDNS name
# For mDNS on windows, make sure you have "Bonjour Print Services for Windows" installed
#   Find it at: https://developer.apple.com/bonjour/
serial_number = "0617"
mdns_hostname = f"ripkit{serial_number}.local"
sirip = RipKit(mdns_hostname)


""" BASIC USAGE """

""" CAN Port Configuration """
sirip.can.bitrate(1e6)  # Set bus bitrate (default is 1Mbps)

""" Open a CAN socket connection to the device"""
cansock = sirip.can.make_socket()

""" Transmit a message """
cob_id = 0x1234
payload = b'xyz123'
cansock.send_frame(cob_id, payload)

""" Retrieve any buffered receive messages """

while True:
    rx_cob_id, rx_payload = cansock.recv_frame(timeout=0)
    if rx_cob_id is None:
        print("All buffered can data has been read!")
        break
    print(f"received id: {rx_cob_id}, payload: {rx_payload}")


""" Start another thread to listen in for receive messages as they appear """
import threading


def handle_can_frames():
    print("Listening for can frames...")
    while True:
        rx_cob_id, rx_payload = cansock.recv_frame(timeout=None)  # wait indefinitely for new frames
        print(f"handle_can_frames: received id: {rx_cob_id}, payload: {rx_payload}")


rx_thread = threading.Thread(target=handle_can_frames)
rx_thread.daemon = True
rx_thread.start()

# input("press enter to quit\n")
cob_id = 0x1234
payload = bytearray(b'xyz123')
while True:
    input("Press enter to send can >")
    cob_id += 1
    payload[5] += 1
    cansock.send_frame(cob_id, payload)
