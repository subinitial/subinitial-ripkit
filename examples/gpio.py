# -*- coding: UTF-8 -*-
# SiRIP Kit GPIO example
# © 2012-2020 Subinitial LLC. All Rights Reserved
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
# following conditions are met: Redistributions of source code and binary forms must retain the above copyright notice,
# these conditions, and the disclaimer below all in plain text. The name Subinitial may not be used to endorse or
# promote products derived from this software without prior written consent from Subinitial LLC. This software may only
# be redistributed and used with a Subinitial Stacks product.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. Refer to the included LICENSE.txt for more details.

from subinitial.ripkit import RipKit

""" Connect to the device """
# You can access the device by IP address (e.g. 192.168.1.49) or mDNS name
# For mDNS on windows, make sure you have "Bonjour Print Services for Windows" installed
#   Find it at: https://developer.apple.com/bonjour/
serial_number = "0617"
mdns_hostname = f"ripkit{serial_number}.local"
sirip = RipKit(mdns_hostname)


""" BASIC USAGE """

""" Set and clear a single pin """
gpio = sirip.gpio
gpio.direction.outputs(gpio.PIN_1)
gpio.set(gpio.PIN_1)
gpio.clear(gpio.PIN_1)

""" Read state of a single pin """
if gpio.get_pin_state(gpio.PIN_1):
    print("GPIO_1 is high")
else:
    print("GPIO_1 is low")

""" Read current status (high/low) of each pin regardless of input/output state """
print("GPIO pin status", bin(gpio.get_state()))

""" Check the pin configuration: inputs vs outputs"""
print("GPIO IO config:", hex(gpio.direction.get()))


""" ADVANCED USAGE """

""" Define a set of pins for use in function calls later """
pins_4_and_5 = gpio.PIN_4 | (1 << 5)

""" Configure GPIO4 and 5 as an output """
gpio.direction.outputs(pins_4_and_5)

""" Command GPIO4 and 5 to output high """
gpio.set(pins_4_and_5)

""" Command GPIO4 and 5 to output low """
gpio.clear(pins_4_and_5)

""" Command GPIO4 high and ALL OTHER PINS LOW! """
gpio.write(gpio.PIN_4)

""" Toggle GPIO5 output state"""
gpio.toggle(gpio.PIN_5)

""" Configure pin 1 as input with a 50kOhm pull-up """
gpio.direction.inputs(gpio.PIN_1)
gpio.configure_pull(gpio.PIN_1, gpio.PULL_UP)
if gpio.get_pin_state(gpio.PIN_1):
    print("GPIO_1 is high")
else:
    print("GPIO_1 is low")
gpio.configure_pull(gpio.PIN_1, gpio.PULL_NONE)