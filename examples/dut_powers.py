# -*- coding: UTF-8 -*-
# SiRIP Kit DUT Power example
# © 2012-2020 Subinitial LLC. All Rights Reserved
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
# following conditions are met: Redistributions of source code and binary forms must retain the above copyright notice,
# these conditions, and the disclaimer below all in plain text. The name Subinitial may not be used to endorse or
# promote products derived from this software without prior written consent from Subinitial LLC. This software may only
# be redistributed and used with a Subinitial Stacks product.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. Refer to the included LICENSE.txt for more details.

from subinitial.ripkit import RipKit

""" Connect to the device """
# You can access the device by IP address (e.g. 192.168.1.49) or mDNS name
# For mDNS on windows, make sure you have "Bonjour Print Services for Windows" installed
#   Find it at: https://developer.apple.com/bonjour/
serial_number = "0617"
mdns_hostname = f"ripkit{serial_number}.local"
sirip = RipKit(mdns_hostname)


""" BASIC USAGE """

""" Turn on each DUT power """
print("Enabling all DUT Powers...")
sirip.dut_powers.set_12v(True)
sirip.dut_powers.set_5v(True)
sirip.dut_powers.set_3v3(True)
sirip.dut_powers.set_neg12v(True)

""" Get status of each DUT power """
print("DUT = Device Under Test")
print("DUT Power 12V On:", sirip.dut_powers.get_12v())
print("DUT Power 5V On:", sirip.dut_powers.get_5v())
print("DUT Power 3.3V On:", sirip.dut_powers.get_3v3())
print("DUT Power -12V On:", sirip.dut_powers.get_neg12v())

""" Turn off each DUT power """
print("Disabling all DUT Powers...")
sirip.dut_powers.set_12v(False)
sirip.dut_powers.set_5v(False)
sirip.dut_powers.set_3v3(False)
sirip.dut_powers.set_neg12v(False)

""" Get status of each DUT power """
print("DUT Power 12V On:", sirip.dut_powers.get_12v())
print("DUT Power 5V On:", sirip.dut_powers.get_5v())
print("DUT Power 3.3V On:", sirip.dut_powers.get_3v3())
print("DUT Power -12V On:", sirip.dut_powers.get_neg12v())